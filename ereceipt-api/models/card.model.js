const sql = require("../db");

//cards detail
const cardDetail = function(card) {
    this.trailing_digits = card.trailing_digits,
    this.leading_digit = card.leading_digit,
    this.card_type = card.card_type,
    this.start_month = card.start_month,
    this.start_year = card.start_year,
    this.expiry_month = card.expiry_month,
    this.expiry_year = card.expiry_year,
    this.customer_id =  card.customer_id
};

/**
 * 
 * @param {*} newCardDetail 
 * @param {*} result 
 * addCard function will insert card details. it will store customers referece 
 */
cardDetail.addCard = (newCardDetail, result) => {
    sql.query("INSERT INTO card_detail SET ?", newCardDetail, (err, res) =>{
        if (err) {
            result(err, null);
            return;
        }
        result(null, { id: res.insertId, ...newCardDetail });
    });
};


/**
 * 
 * @param {*} newCardDetail 
 * @param {*} result 
 */

cardDetail.updateCardDetail = (newCardDetail, result) => {
    const customer_id = newCardDetail['customer_id'];
    //remove the customer id key from json object. 
    delete newCardDetail['customer_id'];
    const newCardDetail1 = JSON.parse(JSON.stringify(newCardDetail), (key, val) => val === null || val === '' ? undefined : val);
    sql.query(`UPDATE card_detail SET ? WHERE customer_id = ${customer_id}`, newCardDetail1, (err, res) =>{
        if (err) {
            result(err, null);
            return;
        }
        result(null, { id: res.insertId, ...newCardDetail });
    });
};


module.exports = cardDetail;
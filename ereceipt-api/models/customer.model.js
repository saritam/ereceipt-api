const sql = require("../db");

//customer details 
const customerDetail = function(customer) {
    this.first_name = customer.first_name;
    this.email = customer.email;
};

/**
 * 
 * @param {*} newCustomer 
 * @param {*} result 
 * addCustomer function will add customer details into database and returns last insertted Id from customer table.
 */
customerDetail.addCustomer = (newCustomer, result) => {
    sql.query("INSERT INTO customer SET ?", newCustomer, (error, res) =>{
        if (error) {
            result(err, null);
            return;
        }else
            result(null, { id: res.insertId, ...newCustomer });
    });
};

/**
 * 
 * @param {*} email 
 * @returns 
 * returns id and email to check if the customer is new
 */
customerDetail.findByCustomerEmail = (email) => {
    const promise =new Promise((resolve, reject) =>{
            const sqlQuery = `SELECT id, email FROM customer WHERE email = '${ email }' `;
            sql.query( sqlQuery , (error, response) => {
            if(error) {
                reject(error);
            }
            else{ 
                if(response.length)
                    resolve(response[0]);
                else
                    resolve(response);
            }
        });
    });
    return promise;
};


/**
 * get the customer and card details based on the partial card details.
 * This function will use to show the email suggetions
 */
 customerDetail.findByPartialCardDetail =  (partialCardDetail) => {
    const promise = new Promise((resolve, reject) =>{
        const sqlQuery = `SELECT * FROM customer c, card_detail cd  WHERE c.id = cd.customer_id  AND ${partialCardDetail} `;
        sql.query(sqlQuery, (error, response) => {
            if(error) {
                reject(error);
            }
            else{ 
                resolve(response);;
            }
        });
    });
    return promise;
};


module.exports = customerDetail;
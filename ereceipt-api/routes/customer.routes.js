module.exports = app => {

    const customerController = require('../controllers/customer.controller');

    var router = require("express").Router();

    router.put("/customer/create", customerController.create);

    router.get("/customer/getcustomers", customerController.getCustomers);

    app.use('/api/', router);
}

const CardDetail = require('../models/card.model');
const Customer = require('../models/customer.model');

/**
 * 
 * @param {*} request 
 * @param {*} resonse 
 * create function will handle the PUT api call. It will check the customer details and card details based on the provided email address.
 * If the email address is not in the database then the record will be stored as a new record or else it will update the existing 
 * card. 
 * 
 */

exports.create = async (request, response) => {
    /**
     * validate request body.
     */
    
    if (!request.body) {
        resonse.status(400).send({
            message: "Can not process the request without contents."
        });
    }

    /**
     * create customer object which will be stored into customer table.
     */

    const customer = new Customer({
        first_name: request.body.first_name,
        email: request.body.email
    });

    /**
     * create card details object which will be stored into partially card details table.
     * 
     */

    const cardDetails = new CardDetail({
        trailing_digits: request.body.trailing_digits,
        leading_digit: request.body.leading_digit,
        card_type: request.body.card_type,
        start_month: request.body.start_month,
        start_year: request.body.start_year,
        expiry_month: request.body.expiry_month,
        expiry_year: request.body.expiry_year,
        customer_id: ""
    });
    
    /**
     * validate the card's trailing digit and email. 
     * 
     */

    if (!customer.email) {
        resonse.status(400).send({
            message: "can not be insert or update customer's detail without customer email address"
        });
    }

    if (!cardDetails.trailing_digits) {
        response.status(400).send({
            message: "can not be insert or update customer's detail without card's trailing digits"
        });
    }

    let identifiedCustomer;
    try {

    /**
     *  validate the email address, check if email is already present in the database. 
     *  function findByCustomerEmail will use to check duplicate entries of email address.
     * 
     */
        identifiedCustomer = await Customer.findByCustomerEmail(customer.email)
        if (identifiedCustomer && identifiedCustomer.email == customer.email)
        {
            //customer_id is a unique key which will be used to update the card details.
            cardDetails.customer_id = identifiedCustomer.id
            CardDetail.updateCardDetail(cardDetails, (error, carddata) =>{
                if (error) {
                    response.status(500).send({
                        message: error.message || "There is some error while updating card details "
                    });
                } else {
                    response.status(200).send({
                        message: 'card details updated successfully!', 
                        customer: carddata
                    });                    
                }
            });
        }

        if (!identifiedCustomer.email) {
            console.log(`Email not found create new record`);

            //save customer's detail
            Customer.addCustomer(customer, (error, data) => {
                if (error) {
                    response.status(500).send({
                        message: error.message || "There is some error while adding customer details"
                    });
                } else {
                    //customer reference will store into card details table.
                    cardDetails.customer_id = data.id

                    //save card's details
                    CardDetail.addCard(cardDetails, (error, carddata) =>{
                        if (error) {
                                response.status(500).send({
                                    message: error.message || "There is some error while adding card details "
                                });
                        } else {
                                response.status(200).send({
                                    message: 'customer details addded successfully!', 
                                    customer: carddata
                                });                    
                        }
                    });
                }
            });
        
        }
    } catch (error) {
        response.status(500).send({
            message: error.message || 'Can not store customer detail, please try again later.'
        });
    }
}

/**
 * 
 * @param {*} request 
 * @param {*} response
 * card's details will be send with request object.
 * Customers list will be displayed in JSON format based on the request parameters
 * request params : trailing_digit, leading_digit, start_date, expiry_date, card type.
 * response will be the json object with customer information and their card details
 * 
 */

 exports.getCustomers = async (request, response) => {
   
    const cardDetailsParams = request.body;
    let cardDetails = ""; 
    let n = 0;

    //format the json object to append it to the select query 
    const count = Object.keys(cardDetailsParams).length;
    for (let key in cardDetailsParams) {
        n++;
        if(cardDetailsParams[key]) {
            if (count > n) {
                cardDetails +=  key  + '= "' + cardDetailsParams[key] + '" AND ';
            } else {
                cardDetails +=  key + '= "' + cardDetailsParams[key]+ '"';
            }           
        }
    }
    try {
        let customerData = await Customer.findByPartialCardDetail(cardDetails);
        if (customerData) {
            response.status(200).send({
                message: "Customers List!",
                customer: customerData
            });
        } else {
            response.status(500).send({
                message: error.message || "There is some error while getting customer and card details "
            });
        }
    } catch (error) { 
            response.status(500).send({
            message: "Unable to find the customers!"
        });
    }
  };

  